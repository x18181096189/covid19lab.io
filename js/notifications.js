function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function featureDetection() {

    if ( ( 'serviceWorker' in navigator ) === false ) {

        return false;

    }

    if ( ( 'PushManager' in window ) === false ) {

        return false;

    }

    return true;

}

function registerServiceWorker() {

    return navigator.serviceWorker.register( '/service-worker.js', {scope: '/'} )
        .then( function( registration ) {

            return registration;

        }).catch( function( err ) {


        });

}

function getNotificationPermissionState() {

    if ( navigator.permissions ) {

        return navigator.permissions.query(

            {

                name: 'notifications'

            }

        ).then( ( result ) => {

            return result.state;

        });

    }

    return new Promise( ( resolve ) => {

        resolve( Notification.permission );

    });

}

function deploySubscription( subscriptionData ) {

    if ( ( 'endpoint' in subscriptionData ) === false ) { return false };

    var firebaseAppJs = document.createElement('script');

    firebaseAppJs.onload = function () {

        var firebaseDatabaseJs = document.createElement('script');

        firebaseDatabaseJs.onload = function () {

            var firebaseConfig = {
                apiKey: "AIzaSyAP2JmdUh10AkT_QYui5mwqtX4vnILF3bs",
                authDomain: "corona-cc4fe.firebaseapp.com",
                databaseURL: "https://corona-cc4fe.firebaseio.com",
                projectId: "corona-cc4fe",
                storageBucket: "corona-cc4fe.appspot.com",
                messagingSenderId: "68160950800",
                appId: "1:68160950800:web:74717b290243b9e5523095"
            };

            firebase.initializeApp(firebaseConfig);

            firebase.database().ref('subscription-entries').push().set({
                endpoint: subscriptionData.endpoint,
                p256dh: subscriptionData.keys.p256dh,
                auth: subscriptionData.keys.auth
            });

        };

        firebaseDatabaseJs.src = 'https://www.gstatic.com/firebasejs/7.11.0/firebase-database.js';

        document.head.appendChild( firebaseDatabaseJs );

    };

    firebaseAppJs.src = 'https://www.gstatic.com/firebasejs/7.11.0/firebase-app.js';

    document.head.appendChild( firebaseAppJs );

}

function denyNotifications( event ) {

    document.getElementById( 'notificationsPrompt' ).classList.remove('active');

    localStorage.setItem( 'lastPrompt', Date.now() );

}

function localStorageDate() {

    if ( ( 'localStorage' in window ) === false ) {

        return 'obsolete';

    }

    var promptLast = localStorage.getItem( 'promptLast' );

    if ( ( Date.now() - parseInt( promptLast ) ) <= 86400000 ) {

        return 'valid';

    } else {

        return 'obsolete';

    }

}

function localStorageState() {

    if ( ( 'localStorage' in window ) === false ) {

        return 'empty';

    }

    var promptState     = localStorage.getItem( 'promptState' );

    if ( promptState === 'granted' ) {

        return 'granted';

    } else if ( promptState === 'denied' ) {

        return 'denied';

    } else if ( promptState === 'delayed' ) {

        return 'delayed';

    } else {

        return 'empty';

    }

}

function localStorageDenied() {

    localStorage.setItem( 'promptState', 'denied' );
    localStorage.setItem( 'promptLast', Date.now() );

}

function localStorageGranted() {

    document.getElementById( 'notificationsPrompt' ).classList.remove('active');

    localStorage.setItem( 'promptState', 'granted' );
    localStorage.setItem( 'promptLast', Date.now() );

}

function localStorageDelayed() {

    document.getElementById( 'notificationsPrompt' ).classList.remove('active');

    localStorage.setItem( 'promptState', 'delayed' );
    localStorage.setItem( 'promptLast', Date.now() );

}




var resp1 = registerServiceWorker();

if ( localStorageState() === 'empty' ) {

    if ( featureDetection() === true ) {

        resp1.then( function( registration ) {

            getNotificationPermissionState().then( function( response ) {

                if ( response === 'granted' ) {

                    localStorageGranted();

                    registration.pushManager.subscribe({
                        userVisibleOnly:          true,
                        applicationServerKey:     urlBase64ToUint8Array('BDgeJE7MWnDK_cIpvXfnoAA-LLSq_jahjdtvndRleQmMn5vDMHIL1cK--VAD77KCEn8WEn3DGXobrZ11Dc8s-wE')
                    }).then( function( pushSubscription ) {

                        localStorageGranted();
                        deploySubscription( pushSubscription.toJSON() );

                    }, function(error) {

                    });

                } else if ( response === 'denied' ) {

                    localStorageDenied();

                } else {

                    setTimeout( function(){

                        document.getElementById( 'denyNotifications' ).addEventListener( 'click', localStorageDelayed );

                        document.getElementById( 'notificationsPrompt' ).classList.add('active');

                        document.getElementById( 'enableNotifications' ).addEventListener( 'click', function( event ) {

                            localStorageGranted();

                            registration.pushManager.subscribe({
                                userVisibleOnly:          true,
                                applicationServerKey:     urlBase64ToUint8Array('BDgeJE7MWnDK_cIpvXfnoAA-LLSq_jahjdtvndRleQmMn5vDMHIL1cK--VAD77KCEn8WEn3DGXobrZ11Dc8s-wE')
                            }).then( function( pushSubscription ) {

                                localStorageGranted();
                                deploySubscription( pushSubscription.toJSON() );

                            }, function(error) {

                            });

                        });

                    }, 10000 );

                }

            });

        });

    } else {

        localStorageDenied();

        resp1.then( function( registration ) {
        });

    }

} else if ( localStorageState() === 'granted' ) {

    resp1.then( function( registration ) {
    });

} else if ( localStorageState() === 'denied' ) {

    resp1.then( function( registration ) {
    });

} else if ( localStorageState() === 'delayed' ) {

    if ( localStorageDate() === 'obsolete' ) {

        if ( featureDetection() === true ) {

            resp1.then( function( registration ) {

                getNotificationPermissionState().then( function( response ) {

                    if ( response === 'granted' ) {

                        localStorageGranted();

                        registration.pushManager.subscribe({
                            userVisibleOnly:          true,
                            applicationServerKey:     urlBase64ToUint8Array('BDgeJE7MWnDK_cIpvXfnoAA-LLSq_jahjdtvndRleQmMn5vDMHIL1cK--VAD77KCEn8WEn3DGXobrZ11Dc8s-wE')
                        }).then( function( pushSubscription ) {

                            localStorageGranted();
                            deploySubscription( pushSubscription.toJSON() );

                        }, function(error) {

                        });

                    } else if ( response === 'denied' ) {

                        localStorageDenied();

                    } else {

                        setTimeout( function(){

                            document.getElementById( 'denyNotifications' ).addEventListener( 'click', localStorageDelayed );

                            document.getElementById( 'notificationsPrompt' ).classList.add('active');

                            document.getElementById( 'enableNotifications' ).addEventListener( 'click', function( event ) {

                                localStorageGranted();

                                registration.pushManager.subscribe({
                                    userVisibleOnly:          true,
                                    applicationServerKey:     urlBase64ToUint8Array('BDgeJE7MWnDK_cIpvXfnoAA-LLSq_jahjdtvndRleQmMn5vDMHIL1cK--VAD77KCEn8WEn3DGXobrZ11Dc8s-wE')
                                }).then( function( pushSubscription ) {

                                    localStorageGranted();
                                    deploySubscription( pushSubscription.toJSON() );

                                }, function(error) {

                                });

                            });

                        }, 10000 );

                    }

                });

            });

        } else {

            localStorageDenied();

            resp1.then( function( registration ) {
            });

        }

    }

}
