var graphData = {};

function createGraph ( data, color ) {

    if ( typeof graph !== 'undefined' ) {

        graph.clearChart();

    }

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( data );

      var options = {
        chartArea: {width: '95%', height: '95%'},
        title: 'Cases',
        legend: { position: 'none' },
        hAxis: { textPosition: 'none', gridlines: { count: 0 }, baselineColor: 'none' },
        vAxis: { format: 'short', textPosition: 'in', gridlines: { minSpacing: 2, color: '#202020' }, minorGridlines: { count: 0, color: '#202020' }, baselineColor: 'none' },
        backgroundColor: '#181818',
        colors: [ color ],
        pointSize: 3
      };

      graph = new google.visualization.LineChart(document.getElementById('graph_chart'));

      graph.draw(dataGoogle, options);

    });

}

function populateChartData( response, field ) {

    var graphRaw = [];
    var fieldCode = null;

    if ( field === 'confirmed' ) {

        graphRaw[0] = [ 'Date', 'Cases' ];
        fieldCode = 'c';

    } else if ( field === 'deaths' ) {

        graphRaw[0] = [ 'Date', 'Deaths' ];
        fieldCode = 'd';

    } else if ( field === 'newconfirmed' ) {

        graphRaw[0] = [ 'Date', 'New Cases' ];
        fieldCode = 'nc';

    } else if ( field === 'newdeaths' ) {

        graphRaw[0] = [ 'Date', 'New Deaths' ];
        fieldCode = 'nd';

    }

    for ( var block in response ) {

        var graphDataLoc = [];
        graphDataLoc.push( block );
        graphDataLoc.push( response[ block ][ fieldCode ] );

        graphRaw.push( graphDataLoc );

    }

    return graphRaw;

}

function createGraphData( response, field, country ) {

    if ( typeof country === 'undefined' || country === '' ) {

        country = 'world';

    }

    var color = '#eee';

    if ( field === 'deaths' ) {

        color = '#FF3F3F';

    } else if ( field === 'newdeaths' ) {

        color = '#FF3F3F';

    }

    if ( country === 'world' ) {

        createGraph( populateChartData( response, field ), color );

    } else {

        var xhr = new XMLHttpRequest();
        xhr.open( 'GET', '/data/graph_history/' + country + '.json', true );

        xhr.onload = function() {

            if ( this.status === 200 ) {

                var response = JSON.parse( this.response );

                createGraph( populateChartData( response, field ), color );

            }

        };

        xhr.send();

    }

}

function manipulateGraph() {

    createGraphData( graphData, 'confirmed' );

}

function graphButtonsReset( event ) {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

    buttons[ 0 ].classList.add('active');

}

function graphButtonsClick( event ) {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].classList.remove('active');

    }

    event.target.classList.add('active');

    if ( document.getElementById( 'controls' ).classList.contains( 'hidden' ) ) {

        createGraphData( graphData, event.target.getAttribute( 'data-show' ), countryCodeCities );

    } else {

        createGraphData( graphData, event.target.getAttribute( 'data-show' ) );

    }

}

function bindGraphButtons() {

    var buttons = document.querySelectorAll( '#graphPanel .mapButton' );
    var buttonsNum = buttons.length;

    for ( var i = 0 ; i < buttonsNum ; i++ ) {

        buttons[ i ].addEventListener( 'click', graphButtonsClick );

    }

}

function load_graphTotal() {

    graphData = JSON.parse(this.responseText);

    google.charts.load('current', {
        'packages':['corechart'],
        'mapsApiKey': 'AIzaSyAh3ImWg6RwxMtYAKSPbGRW4SMZ7e0OwL8'
    });

    google.charts.setOnLoadCallback( manipulateGraph );

}

function get_graphTotal() {

    bindGraphButtons();

    var graphTotalReq = new XMLHttpRequest();
        graphTotalReq.addEventListener("load", load_graphTotal);
        graphTotalReq.open("GET", "/data/graph_total.json");
        graphTotalReq.send();

}
